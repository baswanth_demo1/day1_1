package day1;

public class SavingsAccountClient {
    public static void main(String args[]){
        //ReferenceType handle = new ReferenceType();
        SavingsAccount account = new SavingsAccount();
        SavingsAccount account1 = new SavingsAccount();

        double initialAccountBalance = account.checkBalance();
        double initialAccountBalance1 = account1.checkBalance();
        System.out.printf("Initial account balance is :: %f %n ", initialAccountBalance);
        System.out.printf("Initial account1 balance is :: %f %n ", initialAccountBalance1);

        account.deposit(35000);
        account1.deposit(100000);

        double accountBalanceAfterDeposit = account.checkBalance();
        System.out.printf("Account balance after deposit is :: %f %n ", accountBalanceAfterDeposit);
        double accountBalanceAfterDeposit1 = account1.checkBalance();
        System.out.printf("Account1 balance after deposit is :: %f %n ", accountBalanceAfterDeposit1);

        account.withdraw(20000);

        double accountBalanceAfterWithdraw = account.checkBalance();
        System.out.printf("Account balance after withdrawal is :: %f %n ", accountBalanceAfterWithdraw);
        double accountBalanceAfterWithdraw1 = account1.checkBalance();
        System.out.printf("Account balance after withdrawal is :: %f %n ", accountBalanceAfterWithdraw1);
    }
}
