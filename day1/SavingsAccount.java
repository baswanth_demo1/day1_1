package day1;

public class SavingsAccount {

    private long accountId;
    private String customerName;
    private double accountBalance;

    public void deposit(double amount ){
        this.accountBalance = this.accountBalance + amount;
    }

    public double withdraw(double amount){
        if ( this.accountBalance - amount > 0){
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public double checkBalance(){
        return this.accountBalance;
    }

    public String getCustomerName(){
        return this.customerName;
    }

    public long getCustomerAccountNumber(){
        return this.accountId;
    }
}
